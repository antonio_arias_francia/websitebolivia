﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="MapaRedSocios.aspx.cs" Inherits="SantaNaturaNetworkV3.MapaRedSocios" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/proyecto2/tienda.css" rel="stylesheet" />
    <%--<link href="css/proyecto2/vendors/elegant-icon/style.css" rel="stylesheet" />--%>
    <link href="css/AdminLTE-v1.css?v1" rel="stylesheet" type="text/css" />
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link href="css/tree-table/jquery.treegrid.css?v2" rel="stylesheet" type="text/css" />

    <link href="css/proyecto2/MapaRedSocios.css" rel="stylesheet"/>

    <style>
        

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="margin: 70px auto 0; max-width: 1440px;">
        <p class="titulo">MAPA DE RED</p>
        <div id="page_loader" style="display: none" class="se-pre-con"></div>
        <div class="bloqueFiltrado">
            <div class="datosUsuario">
                <p>Total Empresarios: <strong id="TS"><%=TOTALSOCIOS %></strong></p>
                <p>Empresarios Activos: <strong id="AS"><%=ACTIVOS_SOCIOS %></strong></p>
                <p>Nuevos Empresarios: <strong id="NS"><%=NUEVOS_SOCIOS %></strong></p>
                <p>Empresarios Inactivos: <strong id="IS"><%=INACTIVOS_SOCIOS %></strong></p>
            </div>
            <div id="Div1" class="filtrar">
                <div class="combito">
                    <asp:DropDownList ID="cboPeriodo" CssClass="ddlMapaDeRed" Width="100%" runat="server">
                    </asp:DropDownList>
                </div>
                <div style="display: flex; align-items: center;">
                    <button class="botonFiltrar" type="button" id="btnFiltro" style="outline: none;">FILTRAR</button>
                </div>
            </div>
        </div>

        <div>
            <br />
            <div class="box-body">
                <div class="row" style="width: 100%; margin: 0;">
                    <div class="col-md-12 centerTable" style="padding: 0;">
                        <div class="box box-success table-responsive" style="border: none;">
                            <div class="box box-header" style="border: none;">
                            </div>
                            <div style="margin: 0 82px;">
                                <table id="tbl_red" class="tree content-table table-bordered table-hover text-center table">
                                    <thead>
                                        <tr>
                                            <th style="padding: 12px 16.26px !important;">
                                                <label>Nivel</label></th>
                                            <th>Nombres y Apellidos</th>
                                            <th style="display: none;">Corazones</th>
                                            <th>PP</th>
                                            <th>VIP</th>
                                            <th>VP</th>
                                            <th>VR</th>
                                            <th>VG</th>
                                            <th>VQ</th>
                                            <th>Rango Actual</th>
                                            <th>Rango Máximo</th>
                                            <th>Inscripción</th>
                                            <th>Teléfono</th>
                                            <th>País</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <br />
        </div>
    </div>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="js/tree-table/jquery.cookie.js?v2"></script>
    <script src="js/proyecto2/jqueryDataTablesPremioSocios.js"></script>
    <script src="js/proyecto2/estiloTablasPremioSocios.js"></script>
    <script src="js/tree-table/jquery.treegrid.js"></script>
    <script src="js/tree-table/jquery.treegrid.bootstrap3.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <script src="js/sweetAlert.js" type="text/javascript"> </script>
    <script src="js/EstructuraRed.js?Av35" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.tree').treegrid({
                expanderExpandedClass: 'glyphicon glyphicon-minus',
                    expanderCollapsedClass: 'glyphicon glyphicon-plus',
                'saveState': true,
                'saveStateMethod': 'cookie',
                'saveStateName': 'tree-grid-state'
            })
        });
            window.onload = function () {
                document.getElementById("idMenuRed").style.color = 'white';
                document.getElementById("idMenuRed").style.borderBottom = '3px solid white';

                //document.getElementById("idSubMenuMapaDeRed").style.background = '#2D4A9D';
                document.getElementById("idSubMenuMapaDeRed").style.color = 'white';
                document.getElementById("idSubMenuMapaDeRed").style.fontWeight = "700";

            };
    </script>
</asp:Content>
