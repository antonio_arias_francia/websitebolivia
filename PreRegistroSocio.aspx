﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="PreRegistroSocio.aspx.cs" ClientIDMode="Static" Inherits="SantaNaturaNetworkV3.PreRegistroSocio" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="css/Banner de Store Template/animate.css">
    <link rel="stylesheet" type="text/css" href="css/file-upload.css" />
    <link href="css/estilosDetalleCompra.css" rel="stylesheet" />

    <link href="css/proyecto2/formularioRegistroValidaciones.css" rel="stylesheet" />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/proyecto2/eskju.jquery.scrollflow.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.8.7/dist/sweetalert2.all.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <style>
        /*.anchoBotonGuardar {
            width: 40% !important;
        }*/
        .preRegistroSeleccione {
            margin: 109px 96px 240px;
        }

        .preRegistroSeleccione__titulo label {
            font-weight: 500;
            font-size: 25px;
            color: var(--Dark-primary);
            text-align: start;
            margin: 0;
            line-height: 1;
        }

        .seleccionPais {
            background: #FFFFFF;
            border: 1px solid #B5B5B5;
            box-sizing: border-box;
            box-shadow: 0px 1px 5px #3179CB;
            border-radius: 7px;
            width: min-content;
            margin: 54px 0;
            padding: 39px 143px 54px;
        }

        .seleccionPais__titulo {
            display: flex;
            justify-content: center;
        }

            .seleccionPais__titulo label {
                font-weight: bold;
                font-size: 20px;
                color: var(--Dark-primary);
                margin: 0;
                line-height: 1;
            }

        .seleccionPais .seleccionPais__banderas {
            display: flex;
            grid-gap: 64px;
            margin-top: 79px;
        }

            .seleccionPais .seleccionPais__banderas div {
                display: flex;
                flex-direction: column;
                justify-content: center
            }

            .seleccionPais .seleccionPais__banderas .bandera {
                cursor: pointer;
                background-repeat: no-repeat;
                width: 192PX;
                background-position: inherit;
                background-size: cover;
                transition: .2s background-color;
                height: 192px;
                border-radius: 50%;
                filter: drop-shadow(0px 4px 2px rgba(0, 0, 0, 0.25));
            }

            .seleccionPais .seleccionPais__banderas .banderaPanama {
                background-image: url(img/panama.png);
            }

            .seleccionPais .seleccionPais__banderas .banderaBolivia {
                background-image: url(img/bolivia.png);
            }

            .seleccionPais .seleccionPais__banderas .banderaEcuador {
                background-image: url(img/ecuador.png);
            }

            .seleccionPais .seleccionPais__banderas .banderaEEUU {
                background-image: url(img/usa.png);
            }

            .seleccionPais .seleccionPais__banderas .banderaPeru {
                background-image: url(img/peru.png);
            }

            .seleccionPais .seleccionPais__banderas .bandera:hover {
                background-blend-mode: soft-light;
                background-color: rgb(104,132,211);
            }

            .seleccionPais .seleccionPais__banderas label {
                font-weight: 500;
                font-size: 20px;
                color: var(--Medium-primary);
                text-align: center;
                margin: 22px 0 0 0;
                line-height: 1;
            }

        .preRegistroSocio .preRegistroSocio__titulo1 p {
            font-weight: 900;
            font-size: 25px;
            color: var(--Dark-primary);
        }


        .marginTop {
            width: 270px !important
        }

        .labelPreRegistro {
            font-weight: normal;
            font-size: 16px;
            color: #2D4A9D;
        }

        .form-controlPreRegistro {
            display: block;
            height: calc(4.8rem + 2px);
            width: 260px;
            padding: .375rem 2.15rem;
            font-size: 16px;
            line-height: 1.5;
            color: #3A3A3A;
            font-weight: normal;
            background: #F1FBFF;
            box-shadow: 0px 5px 3px rgba(0, 0, 0, 0.25);
            border-radius: 7px;
            background-clip: padding-box;
            border: none;
            transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
        }

        .noEditable {
            cursor: not-allowed;
            background: #D5E4F4
        }

        select:focus {
            outline: none;
        }

        .gj-datepicker-bootstrap [role=right-icon] button .gj-icon, .gj-datepicker-bootstrap [role=right-icon] button .material-icons {
            top: 14px !important;
        }

        .gj-datepicker-bootstrap [role=right-icon] button {
            height: 100%;
            display: flex;
            align-items: center;
        }

        .form-group .gj-unselectable {
            height: calc(4.8rem + 2px);
            width: 260px !important;
            border-radius: 7px;
            border: none;
            box-shadow: 0px 5px 3px rgb(0 0 0 / 25%);
        }

        .btn-outline-secondary {
            border-radius: 0 7px 7px 0 !important;
            border: 1px solid whitesmoke !important;
        }

        input, button:focus {
            outline: none !important;
        }

        .imgAddPhoto {
            position: absolute;
            width: 40px;
            height: 42px;
            right: 0;
            top: 0;
            cursor: pointer;
            background: rgb(0, 11, 68);
            border-radius: 25%;
        }

        .inputAddPhoto {
            display: none !important;
        }

        .btnCancelar, .btnRegistrar {
            width: 221px;
            height: 56px;
        }

        .btnCancelar {
            background: #FFFFFF;
            border: 1px solid var(--Dark-primary);
            box-sizing: border-box;
            box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
            border-radius: 7px;
            font-weight: 500;
            font-size: 20px;
            color: var(--Dark-primary);
            transition: .2s background;
        }

            .btnCancelar:active {
                border: 1px solid var(--Dark-primary);
                background-color: var(--Light-primary);
                color: var(--Dark-primary);
            }

        .btnRegistrar {
            background: var(--Dark-primary);
            box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
            border: none;
            border-radius: 7px;
            font-weight: 500;
            font-size: 20px;
            color: #FFFFFF;
            transition: .2s background;
        }

            .btnRegistrar:hover {
                background: var(--Medium-primary);
            }

            .btnRegistrar:active {
                background: var(--Light-primary);
            }

        .select {
            padding: 8px 18px;
            color: #333333;
            cursor: pointer;
            border-radius: 5px;
            appareance: none;
            -webkit-appearance: none;
            -moz-appearance: none;
            background-image: url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" width="100" height="50"><polygon points="0,0 100,0 50,50" style="fill:%231B2C5E;" /></svg>  ');
            background-position: right 15px top 50%;
            background-repeat: no-repeat;
            background-size: 15px;
            padding-right: 20px;
        }

            .select:focus, .select:hover {
                outline: none;
            }

        .mensajeContraseñasNoCoinciden {
            font-size: 14px;
            color: #A01C1C;
            position: absolute;
            bottom: -50px;
            font-weight: 500;
            display: flex;
            flex-direction: row;
            align-items: baseline;
            gap: 5px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server">
        <Scripts>
            <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
            <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
            <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
            <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
            <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
            <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
            <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
            <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
        </Scripts>
    </asp:ScriptManager>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <div style="margin: 70px auto 0; max-width: 1440px;">
        <div class="preRegistroSeleccione" id="Banderas">
            <div class="preRegistroSeleccione__titulo">
                <label>Realiza el registro de un nuevo empresario</label>
            </div>
            <div class="seleccionPais">
                <div class="seleccionPais__titulo">
                    <label>SELECCIONA EL PAÍS</label>
                </div>
                <div class="seleccionPais__banderas">
                    <%--<div>
                        <div class="bandera banderaPanama"></div>
                        <label>PANAMA</label>
                    </div>--%>
                    <div>
                        <div class="bandera banderaBolivia"></div>
                        <label>BOLIVIA</label>
                    </div>
                    <%--<div>
                        <div class="bandera banderaEcuador"></div>
                        <label>ECUADOR</label>
                    </div>--%>
                    <div>
                        <div class="bandera banderaEEUU"></div>
                        <label>ESTADOS UNIDOS</label>
                    </div>
                    <div>
                        <div class="bandera banderaPeru"></div>
                        <label>PERÚ</label>
                    </div>
                </div>
            </div>
        </div>

        <div id="ContenidoFluido" class="preRegistroSocio" style="width: 100%; margin: 73px auto 115px; display: grid; grid-template-columns: repeat(15, 1fr); grid-template-rows: 93%; grid-gap: 20px;">

            <div class="row justify-content-md-center" style="grid-column: 2/ span 9; width: 100%; margin: 0; padding: 0; display: block;">
                <!--REGISTRO AFILIACION-->
                <div id="MostrarRegistroCliente">
                    <div class="row form-group colorlib-form" style="background: none; padding: 45px 15px 0;">
                        <div class="form-group scrollflow -pop -opacity" style="margin-bottom: 32px;">
                            <p style="font-weight: bold; font-size: 25px; color: #03164C; line-height: 1; margin: 0;">
                                REGISTRO DE NUEVO EMPRESARIO
                            </p>
                        </div>
                        <div class="form-group">
                            <div style="border: 1px solid #6884D3; border-radius: 7px; padding: 19px 17px 25px; margin-bottom: 31px; box-shadow: 0px 4px 4px rgb(104 132 211 / 25%);">
                                <label style="font-weight: 500; font-size: 22px; color: #1B2C5E; margin-bottom: 35px; line-height: 1;">Información de Afiliación</label>

                                <div style="display: flex; flex-wrap: wrap; gap: 16px; justify-content: space-between;">
                                    <div class="scrollflow -opacity">
                                        <label class="labelPreRegistro">Tipo de Cliente</label>
                                        <select runat="server" id="ddlTC" class="select form-controlPreRegistro text-uppercase">
                                            <option value="01">Socio</option>
                                            <option value="05">Consultor</option>
                                            <option value="03">C. Inteligente</option>
                                        </select>
                                        <%--<asp:DropDownList ID="ddlPaquete" runat="server" CssClass="form-controlPreRegistro text-uppercase" onchange="paqueteCliente(this);" >
                                        <asp:ListItem Value="0"> Seleccione</asp:ListItem>
                                        <asp:ListItem Value="01">Básico</asp:ListItem>
                                        <asp:ListItem Value="02">Profesional</asp:ListItem>
                                        <asp:ListItem Value="03">Empresarial</asp:ListItem>
                                        <asp:ListItem Value="04">Millonario</asp:ListItem>
                                        <asp:ListItem Value="23">Imperial</asp:ListItem>
                                        <asp:ListItem Value="05">Consultor</asp:ListItem>
                                        <asp:ListItem Value="06">C. Inteligente</asp:ListItem>
                                    </asp:DropDownList>--%>
                                    </div>
                                    <div class="scrollflow -opacity">
                                        <label class="labelPreRegistro">Upline</label>
                                        <%--<asp:DropDownList ID="CboUpLine" runat="server" CssClass="form-controlPreRegistro text-uppercase" />--%>
                                        <select runat="server" id="CboUpLine" class="select form-controlPreRegistro text-uppercase"></select>
                                    </div>
                                    <div class="scrollflow -opacity">
                                        <label class="labelPreRegistro">Patrocinador</label>
                                        <asp:TextBox ID="txtPatrocinador" runat="server" CssClass="form-controlPreRegistro noEditable"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row col-md-12">

                                <div class="form-group col-md-3 scrollflow -opacity" style="display: none;">
                                    <label class="labelPreRegistro">Pais de tienda</label>
                                    <asp:DropDownList ID="cboPaisTienda" runat="server" CssClass="form-controlPreRegistro " />
                                </div>
                                <div class="form-group col-md-3 scrollflow -opacity" style="display: none;">
                                    <label class="labelPreRegistro">CDR Preferido</label>
                                    <asp:DropDownList ID="cboTipoEstablecimiento" runat="server" CssClass="form-controlPreRegistro btn-lg " />
                                </div>
                            </div>
                            <div style="border: 1px solid #6884D3; border-radius: 7px; padding: 19px 17px 25px; margin-bottom: 31px; box-shadow: 0px 4px 4px rgb(104 132 211 / 25%);">
                                <label style="font-weight: 500; font-size: 22px; color: #1B2C5E; margin-bottom: 35px; line-height: 1;">Datos Personales</label>

                                <div style="display: flex; flex-wrap: wrap; gap: 14px; justify-content: space-between;">
                                    <div class="form-group scrollflow -opacity">
                                        <label class="labelPreRegistro">Nombres</label>
                                        <asp:TextBox ID="txtNombre" runat="server" CssClass="form-controlPreRegistro text-uppercase " onkeydown="validarLetras(event)"
                                            onkeyup="validarLetras(event)"></asp:TextBox>
                                    </div>
                                    <div class="form-group scrollflow -opacity">
                                        <label class="labelPreRegistro">Primer Apellido</label>
                                        <asp:TextBox ID="txtApPaterno" runat="server" CssClass="form-controlPreRegistro text-uppercase " onkeydown="validarLetras(event)"
                                            onkeyup="validarLetras(event)"></asp:TextBox>
                                    </div>
                                    <div class="form-group scrollflow -opacity">
                                        <label class="labelPreRegistro">Segundo Apellido</label>
                                        <asp:TextBox ID="txtApMaterno" runat="server" CssClass="form-controlPreRegistro text-uppercase " onkeydown="validarLetras(event)"
                                            onkeyup="validarLetras(event)"></asp:TextBox>
                                    </div>


                                    <div class="form-group scrollflow -opacity">
                                        <label class="labelPreRegistro">Género</label>
                                        <select runat="server" id="ComboSexo" class="select form-controlPreRegistro text-uppercase">
                                            <option value="0">Seleccione</option>
                                            <option value="1">MASCULINO</option>
                                            <option value="2">FEMENINO</option>
                                            <option value="3">NO ESPECIFICA</option>
                                        </select>
                                        <%--<asp:DropDownList ID="ComboSexo" runat="server" CssClass="form-controlPreRegistro text-uppercase">
                                        <asp:ListItem Value="0">Seleccione</asp:ListItem>
                                        <asp:ListItem Value="1">MASCULINO</asp:ListItem>
                                        <asp:ListItem Value="2">FEMENINO</asp:ListItem>
                                        <asp:ListItem Value="3">NO ESPECIFICA</asp:ListItem>
                                    </asp:DropDownList>--%>
                                    </div>

                                    <div class="form-group scrollflow -opacity">
                                        <label class="labelPreRegistro">Tipo de documento</label>
                                        <select runat="server" id="ComboTipoDocumento" class="select form-controlPreRegistro text-uppercase">
                                            <option value="0">Seleccione</option>
                                            <option value="1">DOCUMENTO DE IDENTIDAD</option>
                                            <option value="2">PASAPORTE</option>
                                        </select>
                                        <%--<asp:DropDownList ID="ComboTipoDocumento" runat="server" CssClass="form-controlPreRegistro text-uppercase">
                                        <asp:ListItem Value="0">Seleccione</asp:ListItem>
                                        <asp:ListItem Value="1">DOCUMENTO DE IDENTIDAD</asp:ListItem>
                                        <asp:ListItem Value="2">PASAPORTE</asp:ListItem>
                                    </asp:DropDownList>--%>
                                    </div>

                                    <div class="form-group scrollflow -opacity">
                                        <label class="labelPreRegistro">N° Documento</label>
                                        <asp:TextBox ID="txtNumDocumento" runat="server" onkeypress="return validarNumerosPD(event)" CssClass="form-controlPreRegistro text-uppercase "></asp:TextBox>
                                    </div>


                                    <div class="scrollflow -opacity">
                                        <label class="labelPreRegistro">Fecha de nacimiento</label>
                                        <input type="text" id="datepicker" class="form-controlPreRegistro text-uppercase " readonly runat="server" style="height: 100%; background: #F1FBFF; font-size: 16px; padding: 0 20px; border-radius: 7px 0 0 7px; border: none;" />
                                    </div>
                                </div>
                            </div>

                            <div style="border: 1px solid #6884D3; border-radius: 7px; padding: 19px 17px 25px; margin-bottom: 31px; box-shadow: 0px 4px 4px rgb(104 132 211 / 25%);">
                                <label style="font-weight: 500; font-size: 22px; color: #1B2C5E; margin-bottom: 35px; line-height: 1;">Información de Contacto</label>

                                <div style="display: flex; flex-wrap: wrap; gap: 14px; justify-content: space-between;">
                                    <div class="scrollflow -opacity">
                                        <label class="labelPreRegistro">Correo electrónico</label>
                                        <asp:TextBox ID="TxtCorreo" runat="server" CssClass="form-controlPreRegistro" TextMode="Email"></asp:TextBox>
                                    </div>
                                    <div class="scrollflow -opacity">
                                        <label class="labelPreRegistro">Teléfono</label>
                                        <asp:TextBox ID="TxtTelefono" runat="server" onkeypress="return validarNumeros(event)" CssClass="form-controlPreRegistro text-uppercase"></asp:TextBox>
                                    </div>
                                    <div class="scrollflow -opacity">
                                        <label class="labelPreRegistro">Celular</label>
                                        <asp:TextBox ID="TxtCelular" runat="server" onkeypress="return validarNumeros(event)" CssClass="form-controlPreRegistro text-uppercase  solo-numero"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <div style="border: 1px solid #6884D3; border-radius: 7px; padding: 19px 17px 25px; margin-bottom: 31px; box-shadow: 0px 4px 4px rgb(104 132 211 / 25%);">
                                <label style="font-weight: 500; font-size: 22px; color: #1B2C5E; margin-bottom: 35px; line-height: 1;">Información de Residencia</label>

                                <div style="display: flex; flex-wrap: wrap; gap: 14px;">
                                    <div class="scrollflow -opacity" style="display: none">
                                        <label class="labelPreRegistro">Pais de operaciones</label>
                                        <select runat="server" id="cboPais" class="select form-controlPreRegistro text-uppercase"></select>
                                        <%--<asp:DropDownList ID="cboPais" runat="server" CssClass="form-controlPreRegistro text-uppercase" />--%>
                                    </div>
                                    <div class="scrollflow -opacity">
                                        <label class="labelPreRegistro" id="lblEstado">Estado</label>
                                        <select runat="server" id="cboDepartamento" class="select form-controlPreRegistro text-uppercase"></select>
                                        <%--<asp:DropDownList ID="cboDepartamento" runat="server" CssClass="form-controlPreRegistro text-uppercase" />--%>
                                    </div>

                                    <div class="scrollflow -opacity">
                                        <label class="labelPreRegistro" id="lblCondado">Condado</label>
                                        <select runat="server" id="cboProvincia" class="select form-controlPreRegistro text-uppercase"></select>
                                        <%--<asp:DropDownList ID="cboProvincia" runat="server" CssClass="form-controlPreRegistro text-uppercase" />--%>
                                    </div>
                                    <div class="scrollflow -opacity" id="dvDistrito">
                                        <label class="labelPreRegistro" id="lblDistrito">Distrito</label>
                                        <select runat="server" id="cboDistrito" class="select form-controlPreRegistro text-uppercase"></select>
                                        <%--<asp:DropDownList ID="cboDistrito" runat="server" CssClass="form-controlPreRegistro text-uppercase" />--%>
                                    </div>
                                    <div class="scrollflow -opacity" id="blogCdrPremio">
                                        <label class="labelPreRegistro">Y.W. Premio</label>
                                        <select runat="server" id="cboPremio" class="select form-controlPreRegistro text-uppercase"></select>
                                        <%--<asp:DropDownList ID="cboPremio" runat="server" CssClass="form-controlPreRegistro text-uppercase" />--%>
                                    </div>
                                    <div class="scrollflow -opacity">
                                        <label class="labelPreRegistro">Dirección</label>
                                        <asp:TextBox ID="txtDireccion" runat="server" CssClass="form-controlPreRegistro text-uppercase "></asp:TextBox>
                                    </div>
                                    <div class="scrollflow -opacity">
                                        <label class="labelPreRegistro">Referencia de dirección</label>
                                        <asp:TextBox ID="TxtReferencia" runat="server" CssClass="form-controlPreRegistro text-uppercase "></asp:TextBox>
                                    </div>
                                    <div class="scrollflow -opacity" id="bloqPostal">
                                        <label class="labelPreRegistro">** Código Postal</label>
                                        <asp:TextBox ID="txtCodigoPostal" runat="server" CssClass="form-controlPreRegistro text-uppercase " onkeypress="return validarNumeros(event)"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="form-group" style="display: none;">
                            <label style="font-weight: bold; font-size: 16px">DATOS BANCARIOS</label>
                            <div class="row col-md-12">
                                <div class="col-md-4 scrollflow -opacity">
                                    <label class="labelPreRegistro">RUC</label>
                                    <asp:TextBox ID="TxtRUC" runat="server" CssClass="form-controlPreRegistro " onkeypress="return validarNumeros(event)"></asp:TextBox>
                                </div>
                                <div class="col-md-4 scrollflow -opacity">
                                    <label class="labelPreRegistro">Banco</label>
                                    <asp:TextBox ID="TxtBanco" runat="server" CssClass="form-controlPreRegistro text-uppercase " onkeydown="validarLetras(event)"
                                        onkeyup="validarLetras(event)"></asp:TextBox>
                                </div>
                                <div class="col-md-4 scrollflow -opacity">
                                    <label class="labelPreRegistro">N° Cuenta depósito</label>
                                    <asp:TextBox ID="TxtNumCuenDeposito" runat="server" CssClass="form-controlPreRegistro "></asp:TextBox>
                                </div>
                            </div>

                            <div class="row col-md-12">
                                <div class="form-group col-md-6 scrollflow -opacity">
                                    <label class="labelPreRegistro">N° Cuenta detracciones</label>
                                    <asp:TextBox ID="TxtNumCuenDetracciones" runat="server" CssClass="form-controlPreRegistro "></asp:TextBox>
                                </div>
                                <div class="form-group col-md-6 scrollflow -opacity">
                                    <label class="labelPreRegistro">N° Cuenta interbancaria</label>
                                    <asp:TextBox ID="TxtNumCuenInterbancaria" runat="server" CssClass="form-controlPreRegistro "></asp:TextBox>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div style="grid-column: 11/ span 4; margin: 0;">
                <div style="border: 1px solid #6884D3; border-radius: 7px; padding: 22px 17px 64px; margin-bottom: 31px; box-shadow: 0px 4px 4px rgb(0 0 0 / 25%); height: min-content; margin-top: 102px; display: flex; flex-direction: column; height: 616px;">
                    <label style="font-weight: 500; font-size: 22px; color: #1B2C5E; margin-bottom: 26px; line-height: 1; text-align: center">Crear cuenta</label>

                    <div id="formulario" class="formulario" style="display: flex; flex-direction: column; justify-content: center; align-items: center;">
                        <div class="scrollflow -opacity" style="margin: 0 auto 46px; border: 17px solid #000B44; border-radius: 50%; width: 187px; height: 186px;">
                            <%--<label>Mi foto de perfil</label>
                        <div id="imagePreview" class="center-block align-content-center">
                            <img src="img/usuario1.png" class="img-fluid" />
                        </div>--%>
                            <div id="imagePreview" style="background-image: url(img/usuario-azul.png); background-size: cover; background-repeat: no-repeat; width: 100%; height: 100%; border-radius: 50%;">
                            </div>
                            <%--<img style="" width="186" src="img/usuario-azul.png" class="" />--%>
                            <input class="inputAddPhoto" type='file' id="imageUpload" accept=".png, .jpg, .jpeg" />
                            <label for="imageUpload">
                                <img class="imgAddPhoto" src="img/add-photo.png" alt="" /></label>

                            <%--<asp:FileUpload CssClass="form-control imagen" ID="fileUpload1" runat="server" />--%>
                        </div>
                        <%--<div class="form-group col-md-4 scrollflow -opacity" id="btnCargaImagen" runat="server">
                        <label class="label" style="font-weight: bold">Foto de Perfil</label>
                        <label class="file-upload btn btn-success form-control marginTop" style="font-size: 15px">
                            Ingresa tu foto
                                    <asp:FileUpload CssClass="form-control imagen" ID="fileUpload" runat="server" />
                        </label>
                    </div>--%>
                        <div class="scrollflow -opacity" style="margin-bottom: 18px;">
                            <label class="labelPreRegistro">Crear usuario</label>
                            <asp:TextBox ID="txtUl" title="Se necesita un nombre de Usuario" MaxLength="8" runat="server" CssClass="form-controlPreRegistro marginTop"></asp:TextBox>
                        </div>

                        <div class="formulario__grupo" id="grupo__password" style="margin-bottom: 18px;">
                            <label for="password" class="labelPreRegistro formulario__label">Crear contraseña</label>
                            <div class="formulario__grupo-input">
                                <input type="password" class="form-controlPreRegistro marginTop formulario__input" name="password" id="password" maxlength="12" autocomplete="new-password">
                                <%--<asp:TextBox ID="password" runat="server" CssClass="form-controlPreRegistro text-uppercase marginTop formulario__input" MaxLength="12" TextMode="Password"></asp:TextBox>--%>
                                <img id="ojito1" style="position: absolute; right: 20px; bottom: 12px; cursor: pointer;" onclick="mostrarContrasena()"
                                    src="img/ojito.png" alt="" />
                                <i class="formulario__validacion-estado fas fa-times-circle"></i>
                            </div>
                        </div>
                        <div class="formulario__grupo" id="grupo__password2">
                            <label for="password2" class="labelPreRegistro formulario__label">Confirmar contraseña</label>
                            <div class="formulario__grupo-input">
                                <input type="password" class="form-controlPreRegistro marginTop formulario__input" name="password2" id="password2">
                                <img id="ojito2" style="position: absolute; right: 20px; bottom: 12px; cursor: pointer;" onclick="mostrarContrasena2()"
                                    src="img/ojito.png" alt="" />
                                <i class="formulario__validacion-estado fas fa-times-circle"></i>
                            </div>
                            <span class="formulario__input-error">
                                <img width="12" src="img/aviso-error.png" alt="Alternate Text" />Las contraseñas no coinciden. Inténtalo nuevamente.</span>
                        </div>

                        <%--                    <div id="grupo__password" class="scrollflow -opacity " style="margin-bottom: 18px;">
                        <label class="labelPreRegistro formulario__label">Crear contraseña</label>
                        <div class="formulario__grupo-input">
                            <asp:TextBox ID="TxtCl" runat="server" CssClass="form-controlPreRegistro text-uppercase marginTop formulario__input" MaxLength="12" TextMode="Password"></asp:TextBox>
                            <i class="formulario__validacion-estado fas fa-times-circle"></i>
                        </div>
                        <img id="ojito1" style="position: absolute; right: 20px; top: 40px; cursor: pointer;" onclick="mostrarContrasena()"
                            src="img/ojito.png" alt="" />
                    </div>
                    <div id="grupo__password3" class="scrollflow -opacity">
                        <label class="labelPreRegistro">Confirmar contraseña</label>
                        <div class="formulario__grupo-input">
                            <asp:TextBox ID="TxtCl2" runat="server" CssClass="form-controlPreRegistro text-uppercase marginTop formulario__input" MaxLength="12" TextMode="Password"></asp:TextBox>
                            <i class="formulario__validacion-estado fas fa-times-circle"></i>
                            <img id="ojito2" style="position: absolute; right: 20px; top: 40px; cursor: pointer;" onclick="mostrarContrasena2()"
                                src="img/ojito.png" alt="" />
                        </div>
                        <span class="formulario__input-error mensajeContraseñasNoCoinciden">
                            <img width="12" src="img/aviso-error.png" alt="Alternate Text" />Las contraseñas no coinciden. Inténtalo nuevamente.</span>
                    </div>--%>
                    </div>
                </div>
            </div>

            <div class="text-center" style="grid-row: 2; grid-column: 5/ span 7; padding: 0px 65px; height: min-content;">
                <div style="display: flex; justify-content: space-evenly;">
                    <button class="form-controlEPP btnCancelar" type="button" id="btnCancelar">ELIMINAR</button>
                    <button class="form-controlEPP btnRegistrar" type="button" id="btnRegistrar">REGISTRAR</button>
                </div>
            </div>
        </div>

    </div>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" type="text/javascript"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <script src="js/file-uploadv1.js"></script>
    <script src="js/moment.js"></script>
    <script src="js/jquery.numeric.js"></script>
    <script src="js/PreRegistroSocios.js?v15"></script>
    <script></script>

    <script src="js/proyecto2/formularioRegistro.js"></script>

    <script type="text/javascript">
        //Para que el menu del navbar se quede de un color cuando esté seleccionado
        window.onload = function () {
            //document.getElementById("idMenuPreRegistro").style.color = 'white';
            //document.getElementById("idMenuPreRegistro").style.borderBottom = '3px solid white';
        }

        function validarLetras(e) {
            var keyCode = (e.keyCode ? e.keyCode : e.which);
            if (keyCode > 47 && keyCode < 58 || keyCode > 95 && keyCode < 107) {
                e.preventDefault();
            }
        }

        function validarNumeros(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        function pageLoad() {
            $('.file-upload').file_upload();
            $('.solo-numero').numeric();

            function filePreview(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#imagePreview').html("<img src='" + e.target.result + "' style='height:200px' />");
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $('.imagen').change(function () {
                filePreview(this);
            });
            $('#datepicker').datepicker({
                uiLibrary: 'bootstrap4'
            });


        }

        //Para subir una imagen en el icono add-photo
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
                    $('#imagePreview').hide();
                    $('#imagePreview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#imageUpload").change(function () {
            readURL(this);
        });



    </script>

</asp:Content>
