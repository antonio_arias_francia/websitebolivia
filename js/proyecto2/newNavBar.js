﻿(function () {
    const listElements = document.querySelectorAll('.menu__item');
    const list = document.querySelector('.newNavBar-nav');
    const menu = document.querySelector('.menu__hamburguer');

    const addClick = ()=>{
        listElements.forEach(element =>{
            element.addEventListener('click', ()=>{

                let subMenu = element.children[2];
                let height = 0;
                element.classList.toggle('menu__item--active');                

                if (subMenu.clientHeight === 0) {
                    height = subMenu.scrollHeight;
                }

                subMenu.style.height = `${height}px`;
            });
        });
    }

    const deleteStyleHeight = ()=>{
        listElements.forEach(element => {
            //if (element.children[2].getAttribute("style")) {
            //    element.children[2].removeAttribute("style");
            //    element.classList.remove("menu__item--active");
            //}
        });
    }

    window.addEventListener('resize', () => {
        if (window.innerWidth > 1119) {
            deleteStyleHeight();
        } else {
            addClick();
        }
    });

    if (window.innerWidth <= 1119) {
        addClick();
    }

    menu.addEventListener('click', () => list.classList.toggle('menu__links--show'));


})();

